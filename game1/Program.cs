﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Firing angle");
            float theta = float.Parse(Console.ReadLine());
            theta *= (float) (Math.PI / 180);
            Console.WriteLine("Enter Firing speed");
            float speed = float.Parse(Console.ReadLine());
            const float g = 9.8f;
            float vox = speed * (float)Math.Cos(theta);
            float voy = speed * (float)Math.Sin(theta);
            float t = voy / g;
            float h = voy * voy / (2 * g);
            float dx = vox * 2 * t;
            Console.WriteLine("Maximum shell height: " + h);
            Console.WriteLine("Horizontal distance: " + dx);
            Console.ReadKey();
        }
    }
}
